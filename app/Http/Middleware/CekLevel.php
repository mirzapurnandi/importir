<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CekLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$levels)
    {
        if (in_array(Auth::guard()->user()->level, $levels)) {
            return $next($request);
        }
        return response()->json([
            'status' => false,
            'message' => 'Anda tidak memiliki hak akses'
        ], 500);
    }
}
