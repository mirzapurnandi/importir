<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Barang;
use App\Models\BarangMasuk;
use Illuminate\Http\Request;

class LaporanStokMasukController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['ceklevel:admin']);
    }

    public function masuks(Request $request)
    {
        $tgl = Carbon::parse($request->tanggal);
        $tanggal = $tgl->format('Y-m-d');

        $data = BarangMasuk::addSelect([
            'nama_barang' => Barang::select('nama')->whereColumn('barang_id', 'barang.id')->limit(1)
        ]);

        if ($request->kind === 'hari') {
            $data = $data->where('tanggal_masuk', '=', $tanggal)->get();
        } elseif ($request->kind === 'minggu') {
            $tanggal_mulai = $tgl->subWeek()->format('Y-m-d');
            $data = $data->whereBetween('tanggal_masuk', [$tanggal_mulai, $tanggal])->get();
        } elseif ($request->kind === 'bulan') {
            $tanggal_mulai = $tgl->subMonth()->format('Y-m-d');
            $data = $data->whereBetween('tanggal_masuk', [$tanggal_mulai, $tanggal])->get();
        } elseif ($request->kind === 'tahun') {
            $tanggal_mulai = $tgl->subYear()->format('Y-m-d');
            $data = $data->whereBetween('tanggal_masuk', [$tanggal_mulai, $tanggal])->get();
        }

        return $this->successResponse($data, "Menampilkan data Filtering Stok Masuk");
    }
}
