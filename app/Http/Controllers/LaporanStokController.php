<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LaporanStokController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['ceklevel:admin']);
    }

    public function stoks(Request $request)
    {
        $tgl = Carbon::parse($request->tanggal);
        $tanggal = $tgl->format('Y-m-d');

        //$testing = Barang::withSum('barangmasuk', 'qty')->withSum('barangkeluar', 'qty')->get();
        if ($request->kind === 'hari') {
            $filter_masuk = DB::raw("(
                SELECT IFNULL(sum(bm.qty),0)
                FROM barang_masuk bm
                WHERE bm.barang_id = barang.id AND bm.tanggal_masuk = '$tanggal'
            ) as stok_masuk");
            $filter_keluar = DB::raw("(
                SELECT IFNULL(sum(bm.qty),0)
                FROM barang_keluar bm
                WHERE bm.barang_id = barang.id AND bm.tanggal_keluar = '$tanggal'
            ) as stok_keluar");
        } else if ($request->kind === 'minggu') {
            $tanggal_mulai = $tgl->subWeek()->format('Y-m-d');
            $filter_masuk = DB::raw("(
                SELECT IFNULL(sum(bm.qty),0)
                FROM barang_masuk bm
                WHERE bm.barang_id = barang.id AND (bm.tanggal_masuk BETWEEN '$tanggal_mulai' AND '$tanggal')
            ) as stok_masuk");
            $filter_keluar = DB::raw("(
                SELECT IFNULL(sum(bm.qty),0)
                FROM barang_keluar bm
                WHERE bm.barang_id = barang.id AND (bm.tanggal_keluar BETWEEN '$tanggal_mulai' AND '$tanggal')
            ) as stok_keluar");
        } else if ($request->kind === 'bulan') {
            $tanggal_mulai = $tgl->subMonth()->format('Y-m-d');
            $filter_masuk = DB::raw("(
                SELECT IFNULL(sum(bm.qty),0)
                FROM barang_masuk bm
                WHERE bm.barang_id = barang.id AND (bm.tanggal_masuk BETWEEN '$tanggal_mulai' AND '$tanggal')
            ) as stok_masuk");
            $filter_keluar = DB::raw("(
                SELECT IFNULL(sum(bm.qty),0)
                FROM barang_keluar bm
                WHERE bm.barang_id = barang.id AND (bm.tanggal_keluar BETWEEN '$tanggal_mulai' AND '$tanggal')
            ) as stok_keluar");
        } else if ($request->kind === 'tahun') {
            $tanggal_mulai = $tgl->subYear()->format('Y-m-d');
            $filter_masuk = DB::raw("(
                SELECT IFNULL(sum(bm.qty),0)
                FROM barang_masuk bm
                WHERE bm.barang_id = barang.id AND (bm.tanggal_masuk BETWEEN '$tanggal_mulai' AND '$tanggal')
            ) as stok_masuk");
            $filter_keluar = DB::raw("(
                SELECT IFNULL(sum(bm.qty),0)
                FROM barang_keluar bm
                WHERE bm.barang_id = barang.id AND (bm.tanggal_keluar BETWEEN '$tanggal_mulai' AND '$tanggal')
            ) as stok_keluar");
        }

        $stok = DB::table('barang')
            ->select(
                'barang.id',
                'barang.nama',
                $filter_masuk,
                $filter_keluar
            )->get();

        return $this->successResponse($stok, "Menampilkan data Filtering");
    }
}
