<?php

namespace App\Http\Controllers;

use App\Models\BarangMasuk;
use Illuminate\Http\Request;
use App\Exceptions\ImportirException;
use Illuminate\Support\Facades\Validator;

class BarangMasukController extends Controller
{
    public function __construct()
    {
        $this->middleware(['ceklevel:admin,staff']);
    }

    public function masuk(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barang_id' => 'required|exists:barang,id',
            'qty'       => 'required|numeric',
            'tanggal'   => 'required|date_format:Y-m-d'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }

        try {
            $data = new BarangMasuk();
            $data->barang_id     = $request->barang_id;
            $data->qty           = $request->qty;
            $data->tanggal_masuk = $request->tanggal;
            $data->save();

            return $this->successResponse($data, "Stok Barang Masuk berhasil ditambahkan sebanyak $request->qty buah");
        } catch (\Throwable $th) {
            throw new ImportirException('Maaf, terjadi kesalahan saat menambahkan stok barang');
        }
    }
}
