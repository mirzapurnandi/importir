<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Exceptions\ImportirException;
use Illuminate\Support\Facades\Validator;

class BarangController extends Controller
{
    public function __construct()
    {
        $this->middleware(['ceklevel:admin']);
    }

    public function index()
    {
        //$barang = Barang::with(['barangmasuk', 'barangkeluar'])->get();
        $barang = Barang::all();
        return $this->successResponse($barang, 'succcess');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama'          => 'required|string',
            'kategori_id'   => 'required|exists:kategori,id'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }

        try {
            $data = new Barang();
            $data->nama         = $request->nama;
            $data->kategori_id  = $request->kategori_id;
            $data->uuid         = Str::uuid();
            $data->save();

            return $this->successResponse($data, "Barang $data->nama berhasil dibuat");
        } catch (\Throwable $th) {
            throw new ImportirException('Maaf, terjadi kesalahan saat membuat data Barang');
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama'          => 'required|string',
            'kategori_id'   => 'required|exists:kategori,id'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }

        try {
            $data = Barang::findOrFail($request->id);
            $data->nama         = $request->nama;
            $data->kategori_id  = $request->kategori_id;
            $data->save();

            return $this->successResponse($data, "Barang $data->nama berhasil diperbaharui");
        } catch (\Throwable $th) {
            throw new ImportirException('Maaf, terjadi kesalahan saat membuat data Barang');
        }
    }

    public function destroy($barang_id)
    {
        try {
            $data = Barang::findOrFail($barang_id);
            $data->delete();
            return $this->successResponse(null, 'Barang berhasil dihapus');
        } catch (\Throwable $th) {
            throw new ImportirException('Maaf, gagal menghapus Barang');
        }
    }
}
