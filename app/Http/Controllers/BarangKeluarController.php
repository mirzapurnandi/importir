<?php

namespace App\Http\Controllers;

use App\Models\BarangKeluar;
use Illuminate\Http\Request;
use App\Exceptions\ImportirException;
use Illuminate\Support\Facades\Validator;

class BarangKeluarController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['ceklevel:admin']);
    }

    public function keluar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barang_id' => 'required|exists:barang,id',
            'qty'       => 'required|numeric',
            'tanggal'   => 'required|date_format:Y-m-d'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }

        try {
            $data = new BarangKeluar();
            $data->barang_id      = $request->barang_id;
            $data->qty            = $request->qty;
            $data->tanggal_keluar = $request->tanggal;
            $data->save();

            return $this->successResponse($data, "Barang Keluar berhasil diambil sebanyak $request->qty buah");
        } catch (\Throwable $th) {
            throw new ImportirException('Maaf, terjadi kesalahan saat mengurangi stok barang');
        }
    }
}
