<?php

namespace App\Http\Controllers;

use App\Exceptions\ImportirException;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['ceklevel:admin']);
    }

    public function index()
    {
        $kategori = Category::all();
        return $this->successResponse($kategori, 'succcess');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string|unique:kategori,nama,' . $request->id
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }

        try {
            $kategori = new Category();
            $kategori->nama = $request->nama;
            $kategori->save();

            return $this->successResponse($kategori, "Kategori $kategori->nama berhasil dibuat");
        } catch (\Throwable $th) {
            throw new ImportirException('Maaf, terjadi kesalahan saat membuat kategori');
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string|unique:kategori,nama,' . $request->id
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }

        try {
            $kategori = Category::findOrFail($request->id);
            $kategori->nama = $request->nama;
            $kategori->save();

            return $this->successResponse($kategori, "Kategori $kategori->nama berhasil diperbaharui");
        } catch (\Throwable $th) {
            throw new ImportirException('Maaf, terjadi kesalahan saat memperbaharui kategori');
        }
    }

    public function destroy($kategori_id)
    {
        try {
            $kategori = Category::findOrFail($kategori_id);
            $kategori->delete();
            return $this->successResponse(null, 'Kategori berhasil dihapus');
        } catch (\Throwable $th) {
            throw new ImportirException('Maaf, gagal menghapus Kategori');
        }
    }
}
