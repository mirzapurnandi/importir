<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'barang';

    public function kategori()
    {
        return $this->belongsTo(Category::class, 'kategori_id');
    }

    public function barangmasuk()
    {
        return $this->hasMany(BarangMasuk::class, 'barang_id');
    }

    public function barangkeluar()
    {
        return $this->hasMany(BarangKeluar::class, 'barang_id');
    }
}
