<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
            'level' => 'admin',
            'email_verified_at' => now()
        ]);

        DB::table('users')->insert([
            'name' => 'Staff Gudang',
            'email' => 'staff@gmail.com',
            'password' => bcrypt('password'),
            'level' => 'staff',
            'email_verified_at' => now()
        ]);
    }
}
