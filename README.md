<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Installer

-   `git clone https://gitlab.com/mirzapurnandi/importir.git`
-   `cd importir`
-   `composer install`
-   `cp .env-example .env`
-   `php artisan key:generate`
-   `php artisan jwt:secret`
-   `php artisan migrate`
-   `php artisan db:seed`

# PENGGUNAAN POINT API

`{base_url} = 'http://127.0.0.1:8000/'`

## AUTH

### Login

```
    [POST] => {base_url}/api/auth/login
    header {
        "Content-Type": "application/json"
    }

    body {
        "email": "admin@gmail.com",
        "password": "password"
    }
```

### Logout

```
    [POST] => {base_url}/api/auth/logout
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }
```

## KATEGORI

### Daftar Kategori (index)

```
    [GET] => {base_url}/api/kategori/index
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }
```

### Tambah Kategori (store)

```
    [POST] => {base_url}/api/kategori/store
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }

    body {
        "nama": "Kategori Baru"
    }
```

### Edit Kategori (update)

```
    [PUT] => {base_url}/api/kategori/update
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }

    body {
        "id": 2,
        "nama": "Kategori Ganti Nama"
    }
```

### Hapus Kategori (delete)

```
    [DELETE] => {base_url}/api/kategori/delete/{id}
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }
```

## BARANG

### Daftar Barang (index)

```
    [GET] => {base_url}/api/barang/index
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }
```

### Tambah Barang (store)

```
    [POST] => {base_url}/api/barang/store
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }

    body {
        "nama": "Barang Baru",
        "kategori_id": 1
    }
```

### Edit Barang (update)

```
    [PUT] => {base_url}/api/barang/update
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }

    body {
        "id": 1,
        "nama": "Barang Ganti Nama",
        "kategori_id": 1
    }
```

### Hapus Barang (delete)

```
    [DELETE] => {base_url}/api/barang/delete/{id}
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }
```

### Tambah Stok Barang (Barang Masuk)

```
    [POST] => {base_url}/api/barang/masuk
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }

    body {
        "barang_id": 1,
        "qty": 7,
        "tanggal": "2021-01-01"
    }
```

### Kurangi Stok Barang (Barang Keluar)

```
    [POST] => {base_url}/api/barang/keluar
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }

    body {
        "barang_id": 1,
        "qty": 2,
        "tanggal": "2021-01-01"
    }
```

## LAPORAN

### Laporan Stok

```
    [POST] => {base_url}/api/laporan/stok
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }

    body {
        "kind":"tahun", //ganti kind untuk filter berdasarkan (hari, minggu, bulan, tahun)
        "tanggal": "2021-01-01"
    }
```

### Laporan Stok Masuk

```
    [POST] => {base_url}/api/laporan/masuk
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }

    body {
        "kind":"minggu", //ganti kind untuk filter berdasarkan (hari, minggu, bulan, tahun)
        "tanggal": "2021-01-01"
    }
```

### Laporan Stok Keluar

```
    [POST] => {base_url}/api/laporan/keluar
    header {
        "Content-Type": "application/json",
        "Authorization": "Bearer <token>"
    }

    body {
        "kind":"bulan", //ganti kind untuk filter berdasarkan (hari, minggu, bulan, tahun)
        "tanggal": "2021-01-01"
    }
```
