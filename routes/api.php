<?php

use App\Http\Controllers\BarangController;
use App\Http\Controllers\BarangKeluarController;
use App\Http\Controllers\BarangMasukController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LaporanStokController;
use App\Http\Controllers\LaporanStokKeluarController;
use App\Http\Controllers\LaporanStokMasukController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
});

Route::group([
    'middleware' => ['auth:api'],
    'prefix' => 'kategori'
], function ($router) {
    Route::get('index', [CategoryController::class, 'index']);
    Route::post('store', [CategoryController::class, 'store']);
    Route::put('update', [CategoryController::class, 'update']);
    Route::delete('delete/{id}', [CategoryController::class, 'destroy']);
});

Route::group([
    'middleware' => ['auth:api'],
    'prefix' => 'barang'
], function ($router) {
    // Barang
    Route::get('index', [BarangController::class, 'index']);
    Route::post('store', [BarangController::class, 'store']);
    Route::put('update', [BarangController::class, 'update']);
    Route::delete('delete/{id}', [BarangController::class, 'destroy']);

    // Barang Masuk
    Route::post('masuk', [BarangMasukController::class, 'masuk']);

    // Barang Masuk
    Route::post('keluar', [BarangKeluarController::class, 'keluar']);
});

Route::group([
    'middleware' => ['auth:api'],
    'prefix' => 'laporan'
], function ($router) {
    Route::get('stok', [LaporanStokController::class, 'stoks']);
    Route::get('masuk', [LaporanStokMasukController::class, 'masuks']);
    Route::get('keluar', [LaporanStokKeluarController::class, 'keluars']);
});
